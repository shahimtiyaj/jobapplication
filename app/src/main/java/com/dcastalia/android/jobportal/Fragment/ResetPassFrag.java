package com.dcastalia.android.jobportal.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dcastalia.android.jobportal.R;


/**
 * Created by nusrat-pc on 12/15/16.
 */
public class ResetPassFrag extends Fragment {
    Button btn_resend_reset_code;
    Button btn_change_pass_confirm;


    EditText input_reset_code;
    EditText input_new_password;
    EditText input_confirm_password;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.change_pass_layout, container, false);

        input_reset_code = (EditText) view.findViewById(R.id.input_reset_code);
        input_new_password = (EditText) view.findViewById(R.id.input_new_password);
        input_confirm_password = (EditText) view.findViewById(R.id.input_confirm_password);

        btn_resend_reset_code=(Button)view.findViewById(R.id.btn_resend_reset_code);
        btn_change_pass_confirm=(Button)view.findViewById(R.id.btn_change_pass_confirm);


        btn_resend_reset_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getContext(),"Send Request for reset code",Toast.LENGTH_LONG).show();
            }
        });

        btn_change_pass_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getContext(),"Your password has been changed",Toast.LENGTH_LONG).show();
            }
        });


        return view;
    }
}
