package com.dcastalia.android.jobportal.Model;

/**
 * Created by shahimtiyaj on 12/23/2016.
 */

public class Jobs {
    String total_job;
    String title;
    String no_of_vacancy;
    String country;
    String company;
    String position_company;
    String expire_date;


    public Jobs(String total_job, String title, String no_of_vacancy, String country,String company, String position_company,String expire_date) {
        this.total_job = total_job;
        this.title = title;
        this.no_of_vacancy = no_of_vacancy;
        this.country = country;
        this.company=company;
        this.position_company=position_company;
        this.expire_date = expire_date;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTotal_job() {
        return total_job;
    }

    public void setTotal_job(String total_job) {
        this.total_job = total_job;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNo_of_vacancy() {
        return no_of_vacancy;
    }

    public void setNo_of_vacancy(String no_of_vacancy) {
        this.no_of_vacancy = no_of_vacancy;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition_company() {
        return position_company;
    }

    public void setPosition_company(String position_company) {
        this.position_company = position_company;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }



    public Jobs() {

    }





}
