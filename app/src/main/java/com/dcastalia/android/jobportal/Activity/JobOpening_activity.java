package com.dcastalia.android.jobportal.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.dcastalia.android.jobportal.Adapter.CustomListAdapter;
import com.dcastalia.android.jobportal.Controller.AppController;
import com.dcastalia.android.jobportal.Model.Jobs;
import com.dcastalia.android.jobportal.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


/**
 * Created by shahimtiyaj-pc on 12/15/16.
 */

public class JobOpening_activity extends AppCompatActivity {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();
    // job opening json url
    private static final String URL = "http://192.168.1.131/munshi/restAPI/site/joblist?page=1";
    private ProgressDialog pDialog;

    Button btn_search;
    public static boolean isToastShown = false;

    private CustomListAdapter adapter;
    private ListView listView;
    private Context context = JobOpening_activity.this;
    private List<Jobs> jobList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //For Full Screen view----------------------
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Show the Actionbar in the activity
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle("Job Opening");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.job_open_list_item);

        btn_search = (Button) findViewById(R.id.btn_search);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JobOpening_activity.this, Job_search_activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });


//        //Listview adapter----------------------
//        jobList = new ArrayList<Jobs>();
//        adapter = new CustomListAdapter(this, jobList);
//        listView = (ListView) findViewById(R.id.list_job_open);
//        listView.setAdapter(adapter);
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                Intent intent;
//                String title;
//                switch (position) {
//
//                    case 0:
//
//                        break;
//
//                    default:
//                        break;
//                }
//            }
//        });


        // Creating volley request obj
        JsonArrayRequest billionaireReq = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Jobs jobs = new Jobs();
                                jobs.setTotal_job(obj.getString("total_job"));
                                jobs.setTitle(obj.getString("title"));
                                jobs.setPosition_company(obj.getString("position_company"));
                                // jobs.setNo_of_vacancy(obj.getString("no_of_vacancy"));
                                jobs.setCountry(obj.getString("country"));
                               // jobs.setCompany(obj.getString("company"));
                                jobs.setExpire_date(obj.getString("expire_date"));

                                // adding jobs to Jobs array
                                jobList.add(jobs);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                        hidePDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                error.printStackTrace();
                hidePDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(billionaireReq);


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }


    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.finish();
        Intent intent = new Intent(JobOpening_activity.this, MainActivity.class);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        startActivity(intent);
    }



}
