package com.dcastalia.android.jobportal.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.dcastalia.android.jobportal.Fragment.Edit_profile_Frag;
import com.dcastalia.android.jobportal.Fragment.Edit_profile_Frag_camera;
import com.dcastalia.android.jobportal.Fragment.RegFragment_1;
import com.dcastalia.android.jobportal.Fragment.Set_paas_agent_frag;
import com.dcastalia.android.jobportal.Fragment.Set_pass_individual_frag;
import com.dcastalia.android.jobportal.R;


public class MainActivity extends AppCompatActivity {


    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    int flag = 0;
    final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
    int item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //For Full Screen view----------------------
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle("Register Now");

        /**
         *Setup the DrawerLayout and NavigationView
         */

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);

        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();

        mFragmentTransaction.replace(R.id.fragment_container, new RegFragment_1()).commit();


        /**
         * Setup click events on the Navigation View Items.
         */

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                mDrawerLayout.closeDrawers();
                //  toolbar.setTitle("Munshi Enterprise");
                // if (mDrawerLayout.isDrawerVisible(toolbar));


                if (menuItem.getItemId() == R.id.nav_profile) {

                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                    fragmentTransaction.replace(R.id.fragment_container, new Edit_profile_Frag_camera()).commit();

                    setTitle("My Profile");


                }

                if (menuItem.getItemId() == R.id.nav_profile) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                    fragmentTransaction.replace(R.id.fragment_container, new Edit_profile_Frag()).commit();

                    setTitle("My Profile");
                }

                if (menuItem.getItemId() == R.id.nav_job_opening) {

                    Intent intent = new Intent(MainActivity.this, JobOpening_activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);


                }

                if (menuItem.getItemId() == R.id.nav_myjobs) {
                    Intent intent = new Intent(MainActivity.this, Myjob_activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }

                if (menuItem.getItemId() == R.id.nav_fav_job) {
                    Intent intent = new Intent(MainActivity.this, Favourite_activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }

                if (menuItem.getItemId() == R.id.nav_help) {
                    Intent intent = new Intent(MainActivity.this, Help_activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
                if (menuItem.getItemId() == R.id.nav_setting) {
                    Intent intent = new Intent(MainActivity.this, Setting_activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }


                return false;
            }

        });


        /**
         * Setup Drawer Toggle of the Toolbar
         */

        //android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);


        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent.getStringExtra("now") != null) {
            switch (intent.getStringExtra("now")) {
                case "details":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RegFragment_1()).commit();
                    break;
            }
        }

    }

    public void displayFragment(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        String title = "";
        switch (position) {
            case 0:
                fragment = new RegFragment_1();
                title = "Home";
                break;
            case 1:
                fragment = new Set_paas_agent_frag();
                title = "Login";
                break;
            case 2:
                fragment = new Set_pass_individual_frag();
                title = "Restore Password";
                break;

            default:
                break;
        }

        // update selected fragment and title
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment).commit();
            getSupportActionBar().setTitle(title);
            // change icon to arrow drawable
           // getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
        }
    }


}


//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        setTitle("      Register Now");
//
//
////        RegFragment_1 Reg1 = new RegFragment_1();
////        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
////        fragmentTransaction.replace(R.id.fragment_container, Reg1);
////        fragmentTransaction.commit();
////
////
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_profile) {
//
//            // Handle the camera action
//        } else if (id == R.id.nav_job_opening) {
//
//        } else if (id == R.id.nav_myjobs) {
//
//        } else if (id == R.id.nav_fav_job) {
//
//        } else if (id == R.id.nav_help) {
//
//        } else if (id == R.id.nav_setting) {
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }
//
//
//

