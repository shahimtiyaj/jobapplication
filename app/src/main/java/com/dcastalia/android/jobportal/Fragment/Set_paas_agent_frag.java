package com.dcastalia.android.jobportal.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dcastalia.android.jobportal.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nusrat-pc on 12/21/16.
 */
public class Set_paas_agent_frag extends Fragment {


    Button btn_reg3;
    EditText input_password;
    EditText input_confirm_password;
    EditText input_agent_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.set_pass_agent_layout, container, false);

        input_agent_id = (EditText) view.findViewById(R.id.input_agent_id);
        input_password = (EditText) view.findViewById(R.id.input_password);
        input_confirm_password = (EditText) view.findViewById(R.id.input_confirm_password);

        btn_reg3=(Button)view.findViewById(R.id.btn_reg3);

        btn_reg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = input_password.getText().toString();
                String repassword = input_confirm_password.getText().toString();

                if (input_agent_id.getText().toString().length() == 0) {
                    input_agent_id.setError("Agent Number Empty");
                    input_agent_id.requestFocus();
                }

                else if (input_password.getText().toString().length() == 0) {
                    input_password.setError("Please enter password");
                    input_password.requestFocus();
                }

               else if(input_password.getText().toString().length()<8 &&!isValidPassword(input_password.getText().toString())){
                    Toast.makeText(getContext(), "Please enter strong password", Toast.LENGTH_LONG).show();

                }


                else  if (input_confirm_password.getText().toString().length() == 0) {
                    input_confirm_password.setError("Please confirm your password");
                    input_confirm_password.requestFocus();
                }


                else if (!checkPassWordAndConfirmPassword(password, repassword)) {

                    Toast.makeText(getContext(), "Password don't match", Toast.LENGTH_LONG).show();
                }

                else {
                    Fragment fragment = new VarifyEmailFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                    fragmentTransaction.replace(R.id.fragment_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }


            }
        });

        return view;
    }

    //Check password and repassword is write or wrong
    public boolean checkPassWordAndConfirmPassword(String password, String repassword) {
        boolean pstatus = false;
        if (repassword != null && password != null) {
            if (password.equals(repassword)) {
                pstatus = true;
            }
        }
        return pstatus;
    }

    //Valid password----------
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
}
