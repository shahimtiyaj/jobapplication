package com.dcastalia.android.jobportal.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.dcastalia.android.jobportal.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegFragment_1 extends Fragment implements View.OnClickListener {

    Button btn_reg1;
    String catagory;
    EditText input_userName;
    EditText input_passport_nubmer;
    EditText input_phone;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.reg_fragment_1, container, false);

        btn_reg1 = (Button) view.findViewById(R.id.btn_reg1);

        input_userName = (EditText) view.findViewById(R.id.input_userName);
        input_passport_nubmer = (EditText) view.findViewById(R.id.input_passport_nubmer);
        input_phone = (EditText) view.findViewById(R.id.input_phone);


        btn_reg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phone = input_phone.getText().toString();

                if (input_userName.getText().toString().length() == 0) {
                    input_userName.setError("Please enter your name");
                    input_userName.requestFocus();
                }
                else if (input_passport_nubmer.getText().toString().length() == 0) {
                    input_passport_nubmer.setError("Please enter your passport number");
                    input_passport_nubmer.requestFocus();
                }

                else   if (input_passport_nubmer.length()!=9)
                {
                    Toast.makeText(getContext(), "Invalid passport number!", Toast.LENGTH_LONG).show();

                }
                else  if (input_phone.getText().toString().length() == 0) {
                    input_phone.setError("Please enter your phone number");
                    input_phone.requestFocus();
                }

                else if (!isValidPhone(phone)) {
                    Toast.makeText(getContext(), "Phone number not valid!", Toast.LENGTH_LONG).show();

                }
                else if (catagory==null) {
                    Toast.makeText(getContext(), "Select Catagory!", Toast.LENGTH_LONG).show();

                }

                else {

                    radioClick(v);

                }

            }
        });

        /*
    radio button on click event listener
     */
        view.findViewById(R.id.radio_individual_bt).setOnClickListener(this);
        view.findViewById(R.id.radio_agent_bt).setOnClickListener(this);

        return view;
    }


    public void radioClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();

        switch (v.getId()) {
            case R.id.radio_individual_bt:
                if (checked) {

                    btn_reg1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String phone = input_phone.getText().toString();

                            if (input_userName.getText().toString().length() == 0) {
                                input_userName.setError("Please enter your name");
                                input_userName.requestFocus();
                            }
                            else if (input_passport_nubmer.getText().toString().length() == 0) {
                                input_passport_nubmer.setError("Please enter your passport number");
                                input_passport_nubmer.requestFocus();
                            }

                            else   if (input_passport_nubmer.length()!=9)
                            {
                                Toast.makeText(getContext(), "Invalid passport number!", Toast.LENGTH_LONG).show();

                            }
                            else  if (input_phone.getText().toString().length() == 0) {
                                input_phone.setError("Please enter your phone number");
                                input_phone.requestFocus();
                            }

                            else if (!isValidPhone(phone)) {
                                Toast.makeText(getContext(), "Phone number not valid!", Toast.LENGTH_LONG).show();

                            }

                            else {


                                Fragment fragment = new Set_pass_individual_frag();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                                fragmentTransaction.replace(R.id.fragment_container, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }


                        }
                    });
                    catagory = "individual";
                }
                else {
                    Toast.makeText(getContext(), "Select catagory!", Toast.LENGTH_LONG).show();

                }
                break;

            case R.id.radio_agent_bt:
                if (checked) {
                    btn_reg1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String phone = input_phone.getText().toString();

                            if (input_userName.getText().toString().length() == 0) {
                                input_userName.setError("Please enter your name");
                                input_userName.requestFocus();
                            }
                            else if (input_passport_nubmer.getText().toString().length() == 0) {
                                input_passport_nubmer.setError("Please enter your passport number");
                                input_passport_nubmer.requestFocus();
                            }

                            else   if (input_passport_nubmer.length()!=9)
                            {
                                Toast.makeText(getContext(), "Invalid passport number!", Toast.LENGTH_LONG).show();

                            }
                            else  if (input_phone.getText().toString().length() == 0) {
                                input_phone.setError("Please enter your phone number");
                                input_phone.requestFocus();
                            }

                            else if (!isValidPhone(phone)) {
                                Toast.makeText(getContext(), "Phone number not valid!", Toast.LENGTH_LONG).show();

                            }

                            else {
                                Fragment fragment = new Set_paas_agent_frag();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                                fragmentTransaction.replace(R.id.fragment_container, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }

                        }
                    });
                    catagory = "agent";
                }


                break;
            default:
                break;
        }

    }

    /*
    radio button on click method
     */
    @Override
    public void onClick(View v) {
        radioClick(v);
    }

//    //Validate phone number--
//    private boolean isValidMobile(String input_phone) {
//        return android.util.Patterns.PHONE.matcher(input_phone).matches();
//    }

    public static boolean isValidPhone(String phone)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }
    }



}