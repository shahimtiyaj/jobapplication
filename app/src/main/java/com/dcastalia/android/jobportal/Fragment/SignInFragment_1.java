package com.dcastalia.android.jobportal.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dcastalia.android.jobportal.R;


public class SignInFragment_1 extends Fragment {
    Button btn_forgot_pass;
    Button btn_signIn;

    EditText input_email;
    EditText input_password;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("Team B");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.sign_in_fragment_1, container, false);


        input_email = (EditText) view.findViewById(R.id.input_email);
        input_password = (EditText) view.findViewById(R.id.input_password);

        btn_forgot_pass=(Button)view.findViewById(R.id.btn_forgot_pass);
        btn_signIn=(Button)view.findViewById(R.id.btn_signIn);

        btn_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new RecoverPassFrag();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        btn_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getContext(),"Welcome Page",Toast.LENGTH_LONG).show();

            }
        });

        return view;
    }

}
