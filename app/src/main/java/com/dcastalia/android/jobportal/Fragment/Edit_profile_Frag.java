package com.dcastalia.android.jobportal.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dcastalia.android.jobportal.ContentProvider.MyFileContentProvider;
import com.dcastalia.android.jobportal.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by imtiyaj-pc on 12/15/16.
 */
public class Edit_profile_Frag extends Fragment implements View.OnClickListener {

    private static final int REQUEST_CODE = 1;
    private static final int SELECT_PHOTO = 100;
    private static final int CAMERA_REQUEST = 1;
    private static final int CHOOSE_IMAGE_REQUEST = 1;
    private static final int PICK_FROM_GALLERY = 2;

    //----------------------------------------------
    private static final int SELECT_PICTURE = 100;
    private static final String TAG = "MainActivity";
    private final int CAMERA_RESULT = 1;
    //----------------------------------------------
    private String UPLOAD_URL = "http://shahimtiyaj94.comule.com/upload.php";

    private String KEY_IMAGE = "image";
    private String KEY_NAME = "name";
    //-------------------------------------------------

    private final String Tag = getClass().getName();
    Context context;
    Button btn_pic_upload;
    Button btn_edit;
    Button btn_profile_update;
    String gender;
    Camera camera;
    private ImageView imageView;
    private Bitmap mBitmap;
    private EditText inputProfession;
    private EditText inputBirthDate;
    //DatePickerDialog listener
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            inputBirthDate.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(year));
        }
    };
    private EditText inputPassport_Number;
    private EditText inputNID_Number;
    private EditText inputEmail;
    private EditText inputPhone_Number;
    private EditText inputAddress;
    private String Image_path;

    public Edit_profile_Frag() {

    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


/*
Male and Female radio button listioner
 */

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.edit_profile_layout, container, false);

        inputProfession = (EditText) view.findViewById(R.id.profession);
        inputPassport_Number = (EditText) view.findViewById(R.id.passport_no);
        inputNID_Number = (EditText) view.findViewById(R.id.nid_no);
        inputEmail = (EditText) view.findViewById(R.id.input_email);
        inputPhone_Number = (EditText) view.findViewById(R.id.phone_no);
        inputAddress = (EditText) view.findViewById(R.id.input_address);
        //find the birth date id
        inputBirthDate = (EditText) view.findViewById(R.id.birth_date);


        final Bitmap bmp = BitmapFactory.decodeFile("/data/data/com.dcastalia.android.job_portal/files/newImage.jpg");
        imageView = (ImageView) view.findViewById(R.id.profile_pic_id);
        imageView.setImageBitmap(bmp);
        imageView.setEnabled(true);


//        final Bitmap bmp = BitmapFactory.decodeFile("/data/data/com.dcastalia.android.job_portal/files/newImage.jpg");
//        CropImageView imageView = (CropImageView)view.findViewById(R.id.profile_pic_id);        imageView.setImageBitmap(bmp);
//        imageView.setAspectRatio(5, 10);
//        imageView.setFixedAspectRatio(true);
//        imageView.setGuidelines(1);
//        imageView.rotateImage(90);
//        imageView.setImageBitmap(bmp);
//        imageView.setEnabled(true);

        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Toast.makeText(getContext(), "Do you want to change your profile?", Toast.LENGTH_LONG).show();
            }
        });


        btn_pic_upload = (Button) view.findViewById(R.id.btn_pic_upload);
        btn_edit = (Button) view.findViewById(R.id.btn_edit);
        btn_profile_update = (Button) view.findViewById(R.id.btn_profile_update);


        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getContext(), "Update Now", Toast.LENGTH_LONG).show();

            }
        });

        btn_profile_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage();
                //Toast.makeText(getContext(), "Your profile sucessfully updated", Toast.LENGTH_LONG).show();
            }
        });


        btn_pic_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //call select image method  for opening gallery and camera
                selectImage();
            }

        });

 /*
    radio button on click event listener
     */
        view.findViewById(R.id.radio_male_bt).setOnClickListener(this);
        view.findViewById(R.id.radio_female_bt).setOnClickListener(this);


        return view;
    }

    public void radioClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();

        switch (v.getId()) {
            case R.id.radio_male_bt:
                if (checked) {
                    gender = "male";
                }
                break;

            case R.id.radio_female_bt:
                if (checked) {
                    gender = "female";
                }
                break;
            default:
                break;
        }

    }

    /*
    radio button on click method
     */
    @Override
    public void onClick(View v) {
        radioClick(v);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inputBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /*
               call show date picker method and .when onclick show the dialogue
                */
                showDatePicker();
            }
        });
    }

    /*
    show date picker method show date picker dialogue
     */
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Log.i(Tag, "Receive the camera result");

        File out = null;
        if (resultCode == Activity.RESULT_OK && requestCode == CAMERA_RESULT) {

            out = new File(getActivity().getFilesDir(), "newImage.jpg");

            Toast.makeText(getActivity().getBaseContext(),

                    "Image captured and stored successfully", Toast.LENGTH_LONG)

                    .show();

            if (!out.exists()) {

                Toast.makeText(getActivity().getBaseContext(), "Error while capturing image", Toast.LENGTH_LONG).show();

                return;

            }

            super.onActivityResult(requestCode, resultCode, data);

            Bitmap mBitmap = BitmapFactory.decodeFile("/data/data/com.dcastalia.android.job_portal/files/newImage.jpg");

            imageView.setImageBitmap(mBitmap);

        } else {

            if (resultCode == Activity.RESULT_OK) {

                Uri selectedImage = data.getData();

                String[] filePath = {MediaStore.Images.Media.DATA};

                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);

                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePath[0]);

                String picturePath = c.getString(columnIndex);

                c.close();

                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));

                saveToInternalStorage(thumbnail);

                Toast.makeText(getActivity().getBaseContext(), "Gallery image selected and saved successfully", Toast.LENGTH_LONG).show();

                Bitmap mBitmap = BitmapFactory.decodeFile("/data/data/com.dcastalia.android.job_portal/files/newImage.jpg");

                imageView.setImageBitmap(mBitmap);

            }
        }

    }

    //Save gallery image in app data directory
    private String saveToInternalStorage(Bitmap bitmapImage) {

        File mFile = new File(getContext().getFilesDir(), "newImage.jpg");


        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mFile);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mFile.getAbsolutePath();
    }

//    public String saveToInternalStorage(Bitmap bmp) {
//
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] imageBytes = baos.toByteArray();
//        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
//        return encodedImage;
//    }

    /*
    Select image from gallery and camera
     */

    public void onDestroy() {

        super.onDestroy();

        imageView = null;

    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {

                    PackageManager pm = getActivity().getPackageManager();

                    if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                        startActivityForResult(i, CAMERA_RESULT);


//                        i.putExtra("crop", "true");
//                        i.putExtra("aspectX", 0);
//                        i.putExtra("aspectY", 0);
//                        i.putExtra("outputX", 200);
//                        i.putExtra("outputY", 150);
//
//                        try {
//
//                            i.putExtra("return-data", true);
//                            startActivityForResult(i, CAMERA_RESULT);
//
//                        } catch (ActivityNotFoundException e) {
//
//                        }




                    } else {

                        Toast.makeText(getActivity().getBaseContext(), "Camera is not available", Toast.LENGTH_LONG).show();

                    }

                } else if (options[item].equals("Choose from Gallery")) {

                    //  callGallery();

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                }

                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


/*
open gallery method
 */

    public void callGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 0);
        intent.putExtra("aspectY", 0);
        intent.putExtra("outputX", 200);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(
                Intent.createChooser(intent, "Complete action using"),2);

    }


    //onPictureTaken is picture taken image rotation
    public void onPictureTaken(byte[] data, Camera camera) {

        //String timeStamp = new SimpleDateFormat( "yyyyMMdd_HHmmss").format( new Date( ));
        // output_file_name = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + timeStamp + ".jpeg";

        File pictureFile = new File(getActivity().getFilesDir(), "newImage.jpg");
        if (pictureFile.exists()) {
            pictureFile.delete();
        }

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);

            Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);

            ExifInterface exif = new ExifInterface(pictureFile.toString());

            Log.d("EXIF value", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
            if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")) {
                realImage = rotate(realImage, 90);
            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")) {
                realImage = rotate(realImage, 270);
            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")) {
                realImage = rotate(realImage, 180);
            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")) {
                realImage = rotate(realImage, 90);
            }

            boolean bo = realImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);

            fos.close();

            ((ImageView) imageView.findViewById(R.id.imageView)).setImageBitmap(realImage);

            Log.d("Info", bo + "");

        } catch (FileNotFoundException e) {
            Log.d("Info", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("TAG", "Error accessing file: " + e.getMessage());
        }
    }

    /*
DatePickerFragment is the inner class inside the edit profile fragment

 */
    public static class DatePickerFragment extends DialogFragment {
        DatePickerDialog.OnDateSetListener ondateSet;
        private int year, month, day;

        public DatePickerFragment() {


        }

        public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
            ondateSet = ondate;
        }

        @SuppressLint("NewApi")
        @Override
        public void setArguments(Bundle args) {
            super.setArguments(args);
            year = args.getInt("year");
            month = args.getInt("month");
            day = args.getInt("day");
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new DatePickerDialog(getActivity(), ondateSet, year, month, day);
        }
    }


    //Uploading image from mobile to server using volley---------------

    private void uploadImage() {

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d(TAG, "Upload Response: " + s);
                        loading.dismiss();
                        //Showing toast message of the response
                        Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Log.e(TAG, "Uploading Error: " + error.getMessage());
                        Toast.makeText(getContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                })


        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = saveToInternalStorage(mBitmap);

                //Getting Image Name
                String name = inputProfession.getText().toString().trim();

                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put(KEY_IMAGE, image);
                params.put(KEY_NAME, name);

                //returning parameters
                return params;
            }
        };


        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);

    }

////Convert Bitmap image into Base64 string ----------------------
//    public String getStringImage(Bitmap bitmapImage) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//        bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] imageBytes = baos.toByteArray();
//        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
//        return encodedImage;
//    }


}

